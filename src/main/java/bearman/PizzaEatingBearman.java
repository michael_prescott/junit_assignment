package bearman;

import food.Food;

public class PizzaEatingBearman extends Bearman{
	
	public void eat(Food food) {
		food.feedBearman(this);
	}
	
}


