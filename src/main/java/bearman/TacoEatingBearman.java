package bearman;

import food.Food;

public class TacoEatingBearman extends Bearman{
	
	public void eat(Food food) {
		food.feedBearman(this);
	}

}
